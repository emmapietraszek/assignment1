*Fleet Planning

SETS
v set of ships /v1*v5/
r set of routs /Asia,ChinaPacific/
p set of ports /Singapore,Incheon,Shanghai,Sydney,Gladstone,Dalian,Osaka,Victoria/
h1(p) subset of ports that are incompatible /Singapore,Osaka/
h2(p) subset of ports that are incompatible /Incheon,Victoria/
;

SCALAR
M upper bound on the amount of times a ship sails each of the routs added /365/
K the number of ports the company must minimum visit /5/;

PARAMETERS
F(v) the fixed cost of using ship v in million dollars
         /v1 65, v2 60,v3 92,v4 100,v5 110/
G(v) maximum days a ship can sail a year
         /v1 300, v2 250,v3 350,v4 330,v5 300/
D(p) the amount of days a port has to be visited a year if the company decides to visit it
         /Singapore 15,Incheon 18,Shanghai 32,Sydney 32,Gladstone 45,Dalian 32,Osaka 15,Victoria 18/
;

TABLE C(v,r) cost of ship v to complete route r
              Asia   ChinaPacific
         v1   1.41      1.9
         v2   3.0       1.5
         v3   0.4       0.8
         v4   0.5       0.7
         v5   0.7       0.8
;

TABLE T(v,r) days needed for ship v to complete route r
              Asia   ChinaPacific
         v1   14.4       21.2
         v2   13.0       20.7
         v3   14.4       20.6
         v4   13.0       19.2
         v5   12.0       20.1
;

TABLE A(p,r) is one if route r passes through port p 0 otherwise
                      Asia    ChinaPacific
         Singapore     1
         Incheon       1
         Shanghai      1           1
         Sydney                    1
         Gladstone                 1
         Dalian        1           1
         Osaka         1
         Victoria                  1
;

INTEGER VARIABLE
x(v,r) how many times ship v sailes route r;
*Since ships can't sail only a part of the route we set x to be integer.

BINARY VARIABLE
y(v) indicates if ship v is used or not
w(p) indicades if port p is visited or not;

VARIABLE
z objection function value;

EQUATIONS
         obj             the objection function is the total yearly cost
         Yconst(v)       makes the variable y equal 1 when ship v i used
         maxSail(v)      makes sure each ship do not sail more than they a allowed to
         minVisit(p)     makes sure each port is visited D(p) times if the company decides to visit it
         minPorts        the company must vist a minimum of K ports
         IncomPorts1     the compnay can't visit both of the ports in h1
         IncomPorts2     the company can't visit both of the ports in h2
;

obj ..           z =e= sum((v,r),C(v,r)*x(v,r)) + sum(v,F(v)*y(v));
*The total cost is the cost for each combination of ships and routs if they are used, plus the fixed
*cost of bying the ships.

Yconst(v) ..     y(v)*M =g= sum(r,x(v,r));
*If ship v is used on one or more of the routs we force y to be one.

maxSail(v) ..    sum(r,x(v,r)*T(v,r)) =l= G(v);
*The number of times ship v sails route r, mutiplied by the T-matrix that tells us how many days the ship is
*sailing a route r. IF I sum over alle the routs I get the number of days I use ship v a year. This must not exceed G,
*that is the number of days ship v can sail a year.

minVisit(p) ..   sum((v,r),x(v,r)*A(p,r)) =g= D(p)*w(p);
*The number of times ship v sails route r multiplied with the A-matrix gives the number of times ship v visits
*port p. If I sum over all ships and and routs I get the number of times port p is visited in total. This must be
*greater than the right side, which is the number of times port p must be visited, but only if the company decides
*to visit it. So I multiply it by w(p) which is binary and 1 if port p is visited.

minPorts ..      sum(p,w(p)) =g= K;
*The sum over all the w's must be at least K, so that the company visits at least K ports.

IncomPorts1 ..   sum(h1(p),w(p)) =l= 1;
*The company can't both visit Singapore and Osaka.

IncomPorts2 ..   sum(h2(p),w(p)) =l= 1;
*The company can't both visit Incheon and VIctoria.

Model FleetPlanning /all/;

Solve FleetPlanning using mip minimizing z;

Display x.l, y.l, w.l, z.l;







