* Key Management in Wireless Sensor Networks

SETS
i set of nodes /n1*n8/
k set of keys /k1*k30/
;

Alias (i,j);
*I let j be the same set of nodes as i, since I want to look at the connection from one node to another.

SCALARS
T number of times one key can be used /4/
q the number of keys two nodes need to share to be able to connect /3/
e epsilon (a small number used in an inequality) /0.5/
b a upper bound on the maksimum keys two nodes can have in common minus q
;

b = card(k)-q;
*This is a upper bound. It is not the best upper bound as two nodes realisticly can't have all the keys in common,
*but it makes my inequality work in a general case of the key management problem.

PARAMETERS
n(k) the space a key k takes up in a note /#k 186/
*This number is 186 kB for all the 30 keys.

M(i) the memory limit of node i /#i 700/
*The limit for all nodes are 700 kB.
;

BINARY VARIABLES
x(i,k) tells if node i contains key k
u(i,j,k) tells if node i and j contains key k
y(i,j) tells if there is a connection from node i to node j
;

VARIABLE
z the objective function value;

EQUATIONS
Obj              I want to maximize the number of connections in the network
MaxKey(k)        Ensures that one key can only be used a certain number of times
Space(i)         Limits the memory that can be used in one node
u1(i,j,k)        Sets u to 1 when key k is in both i and j
u0(i,j,k)        Sets u to 0 otherwise
y1(i,j)          Sets y to 1 when i and j has at least q keys in common
y0(i,j)          Sets y to 0 otherwise
;

Obj ..           z =e= sum((i,j)$(ord(i)<ord(j)), y(i,j));
*Since I want to maximize the number of connections, the objective function is the sum of all the y's over i and j.
*But I set i<j to avoid counting the same connection twice and not to count the connection from a node to it self.

MaxKey(k) ..     sum(i,x(i,k)) =l= T;
*For every key k I say that it can at most be used T times. Since x says if key k is in node i,
*the sum over all the i's must give how many times key k is used.

Space(i) ..      sum(k,x(i,k)*n(k)) =l= M(i);
*For every node i I say that the sum of the space that every key takes up multiplied with x that is one if key k is in i,
*must not exceed the memory limit M(i)

u1(i,j,k) ..     x(i,k) + x(j,k) =l= 1 + u(i,j,k);
*This constraint forces u(i,j,k) to be one when both x(i,k) and x(j,k) are one.

u0(i,j,k) ..     x(i,k) + x(j,k) =g= 2*u(i,j,k);
*Here I force u(i,j,k) to be zero if one or both of x(i,k) and x(j,k) is zero.
*So if one or both of the nodes does not contain key k, u must be zero.

y1(i,j) ..       sum(k,u(i,j,k)) - (b + e)*y(i,j) =l= q - e;
*This constraint forces y to be one if i and j contain more than q keys that are the same.
*It is constructed from the lecture slides "The use of variables" page 11.
*Because of the subtracted e (epsilon) on the right side, when the sum of the u's are at least q, then
*the inequality forces y to be 1 (to make the left side smaller than the right side).
*When the sum of u's are less than q, the inequality does not force y bo be anything.

y0(i,j) ..       q*y(i,j) =l= sum(k,u(i,j,k));
*If the number of keys in common are less than q, I force y to be zero.

Model Key /all/;

Solve Key using mip maximizing z;

Display x.l, u.l, y.l, z.l;


*I notice that this model I have build have a lot of extra vaiables than the ones I maximize in the objective function.
*In the objective function I only sum over the y�s where i<j, but I still have variables defined for i<=j and check
*all the constraints in those cases. So I am aware that the problem could be made smaller.

