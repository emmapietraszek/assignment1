*Optimal Formation

SET
i set of players /P1*P25/
j set of positions /GK,CDF,LB,RB,CMF,LW,RW,OMF,CFW,SFW/
k set of formations /442,352,4312,433,343,4321/
q(i) subset of quality players /P13,P20,P21,P22/
s(i) subset of strength players /P10,P12,P23/
;

SCALAR
M number of quality players minus one;
*This scalar is used in a inequality later.

M = sum(q(i),1) - 1;
*Since GAMS can't use card() on subsets I constructed this sum instead.


TABLE a(k,j) number of players required for each position in the different formations
               GK   CDF   LB   RB   CMF   LW   RW   OMF   CFW   SFW
         442   1     2    1    1     2    1    1     0     2     0
         352   1     3    0    0     3    1    1     0     1     1
        4312   1     2    1    1     3    0    0     1     2     0
         433   1     2    1    1     3    0    0     0     1     2
         343   1     3    0    0     2    1    1     0     1     2
        4321   1     2    1    1     3    0    0     2     1     0
;

TABLE b(i,j) fitness level of the players in the differnt positions
               GK   CDF   LB   RB   CMF   LW   RW   OMF   CFW   SFW
          P1   10
          P2   9
          P3   8.5
          P4         8    6    5    4     2    2
          P5         9    7    3    2          2
          P6         8    7    7    3     2    2
          P7         6    8    8          6    6
          P8         4    5    9          6    6
          P9         5    9    4          7    2
         P10         4    2    2    9     2    2
         P11         3    1    1    8     1    1     4
         P12         3         2    10    1    1
         P13                        7                10    6
         P14                        4     8    6     5
         P15                        4     6    9     6
         P16                              7    3
         P17                        3          9
         P18                                         6     9     6
         P19                                         5     8     7
         P20                                         4     4     10
         P21                                         3     9     9
         P22                                               8     8
         P23         3    1    1    8     4    3     5
         P24         3    2    4    7     6    5     6     4
         P25         4    2    2    6     7    5     2     2
;

BINARY VARIABLES
x(i,j) is one if player i is chosen for position j
y(k) is one if position k is chosen
;

VARIABLE
z objection function value;

EQUATIONS
         obj             the objection function is the total fitness level of the players chosen to play
         OnePos(i)       each player can at most play one position
         OneForm         we must choose exacly one formation
         FormConst(j)    we must choose exacly the amount of players for each position that the chosen formation describes
         OneQual         we must choose at least one quality player
         QualStre        if we choose all qualty players we must use at least one strength player
;

obj ..           z =e= sum((i,j),x(i,j)*b(i,j));
*The objective function is the binary varible x muliplied by the fitness level of the player in that position.
*In this way we only add the fitness level of the players we choose in the position we coose them to play.

OnePos(i) ..     sum(j,x(i,j)) =l= 1;
*If we sum x over all the positions j, this sum must be at most one for every player i,
*since a player can only play one position.

OneForm ..       sum(k,y(k)) =e= 1;
*Exacly one of the y's must be one, so that the coach chooses one formation.

FormConst(j) ..  sum(k,y(k)*a(k,j)) =e= sum(i,x(i,j));
*Since only one y is one, we must have for that formation k, that the a-matrix fits the number of players playing
*each position.

OneQual ..       sum((q(i),j),x(i,j)) =g= 1;
*This forces at least one guality player to play, since I sum over the subset of quality players.

QualStre ..      sum((q(i),j),x(i,j)) =l= sum((s(i),j),x(i,j)) + M;
*Here I force at least one of the strength players to be used if all of the quality players is being used.
*This is only the case because M is set to be the number of guality players minus one.


Model OptimalFormation /all/;

Solve OptimalFormation using mip maximizing z;

Display x.l, y.l, z.l;


